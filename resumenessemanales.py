#!/usr/bin/python
# -*- coding: utf-8 -*-

from errbot import BotPlugin, botcmd, arg_botcmd, webhook
import shelve

class Resumenessemanales(BotPlugin):
    """
    Historias que han ocurrido durante la ultima semana en Makespace Madrid
    """

    # Passing split_args_with=None will cause arguments to be split on any kind
    # of whitespace, just like Python's split() does
    # @botcmd(split_args_with=None)
    # def example(self, message, args):
    #    """A command which simply returns 'Example'"""
    #    return "Example"

    # @arg_botcmd('name', type=str)
    # @arg_botcmd('--favorite-number', type=int, unpack_args=False)
    # def hello(self, message, args):
    #    """
    #    A command which says hello to someone.
    #
    #    If you include --favorite-number, it will also tell you their
    #    favorite number.
    #    """
    #    if args.favorite_number is None:
    #        return "Hello {name}".format(name=args.name)
    #    else:
    #        return "Hello {name}, I hear your favorite number is {number}".format(
    #            name=args.name,
    #            number=args.favorite_number,
    #        )


    # Absolute Work in progress - Don't get crazy about it not working! :P
    @botcmd
    def resumen(self, message,args):
        """ Comando que te pregunta por algo que ha ocurrido esta semana"""
        return "Cuentame que ha pasado esta semana escribiendo resumen comunidad, resumen asociacion, resumen espacio, resumen actividades o resumen otros. Si quieres ver lo que ya está apuntado puedes hacerlo escribiendo resumen leer"

    @botcmd
    def resumen_comunidad(self, message, args):
        """ Comando que agrega una frase relacionada con la comunidad"""
        with open("resumensemana.txt", "a") as f:
            data = "comunidad - %s.\n" % (args)
            f.write(data)
        return "Gracias! Me apunto %s" % (data)

    @botcmd
    def resumen_asociacion(self, message, args):
        """ Comando que agrega una frase relacionada con la asociacion"""
        with open("resumensemana.txt", "a") as f:
            data = "asociacion - %s.\n" % (args)
            f.write(data)
        return "Gracias! Me apunto %s" % (data)

    @botcmd
    def resumen_espacio(self, message, args):
        """ Comando que agrega una frase relacionada con el espacio"""
        with open("resumensemana.txt", "a") as f:
            data = "espacio - %s.\n" % (args)
            f.write(data)
        return "Gracias! Me apunto %s" % (data)

    @botcmd
    def resumen_actividades(self, message, args):
        """ Comando que agrega una frase relacionada con las actividades"""
        with open("resumensemana.txt", "a") as f:
            data = "actividades - %s. \n" % (args)
            f.write(data)
        return "Gracias! Me apunto %s" % (data)

    @botcmd
    def resumen_otros(self, message, args):
        """ Comando que agrega una frase relacionada con otros temas"""
        with open("resumensemana.txt", "a") as f:
            data = "otros - %s. \n" % (args)
            f.write(data)
        return "Gracias! Me apunto %s" % (data) 

    @botcmd
    def resumen_leer(self, message, args):
        """ Comando que recupera los contenidos del resumen de la semana actual"""
        with open("resumensemana.txt", "r") as f:
            data = f.read()
        cabecera = "# Resumen de la semana actual\n"
        data = cabecera + data
        return data
