# Plugin de Resumenes Semanales para Errbot

**Resumenes semanales** es un plugin para [Errbot](http://errbot.io), un robot de chat basado en Python. Este plugin te permite registrar y leer los temas que han ocurrido durante la última semana de forma grupal.

# Requisitos

# Instalación
En una conversación privada con tu bot:
```
!repos install https://www.gitlab.com/makespacemadrid/ResumenesSemanales
```

# Configuracion
El robot no requiere configuracion adicional

# Uso
El robot soporta varios comandos:

* !resumen - devuelve información general sobre el plugin
* !resumen actividades - Comando que agrega una frase relacionada con las actividades
* !resumen espacio - Comando que agrega una frase relacionada con el espacio
* !resumen comunidad - Comando que agrega una frase relacionada con la comunidad
* !resumen asociacion- Comando que agrega una frase relacionada con la asociacion
* !resumen otros - Comando que agrega una frase relacionada con otros temas
* !resumen leer - Comando que recupera los contenidos del resumen de la semana actual

# Desarrollo
Este proyecto está en una etapa inicial y experimental. Es posible que cambien buenas partes del código en siguientes versiones, pero de momento estamos iterando funcionalidades de forma experimental. 

## Agradecimientos
Agradezco a Alina Mackenzie (alimac) sus [magníficos tutoriales](https://alimac.io/writes/adventures-with-errbot-part-3/), que han servido de inspiración para este bot